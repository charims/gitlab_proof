# Gitlab Proof
This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:AD1FE92D9578AD931670A4F84428A10FD56A51C7]
